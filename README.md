# Binary Clock

A quick coffee-break project to make a binary clock in vanilla HTML / CSS / JavaScript.

![screenshot](https://i.imgur.com/JjuJRnU.png)

[Example running](https://blchrd.eu/binary-clock/).
